<?php
/**
 * Plugin Name:       AGP Custom Dashboard
 * Description:       WP customisation snippets for AGP Agência clients.
 * Plugin URI:        https://gitlab.com/agpagencia/agp-custom-dashboard
 * Version:           0.0.1
 * Author:            Everaldo Matias
 * Author URI:        https://everaldo.dev/
 * Requires at least: 3.0.0
 * Tested up to:      4.4.2
 *
 * @package AGP_Custom
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Main AGP_Custom Class
 *
 * @class AGP_Custom
 * @version	0.0.1
 * @since 0.0.1
 * @package	AGP_Custom
 */
final class agp_custom_class {

	/**
	 * Set up the plugin
	 */
	public function __construct() {
		add_action( 'login_enqueue_scripts', array( $this, 'agp_custom_login_admin_css' ) );
		add_action( 'login_footer', array( $this, 'agp_custom_message_login' ) );
	}

	/**
	 * Enqueue the CSS to Login page
	 *
	 * @return void
	 */
	public function agp_custom_login_admin_css() {
		wp_enqueue_style( 'agp-custom-dashboard-css', plugins_url( '/assets/css/agp-custom-dashboard.css', __FILE__ ) );
	}

	/**
	 * Print the message to help users in Login page
	 */
	public function agp_custom_message_login() {
		echo '<div class="help-from-agp">Está com problemas ou dúvidas sobre seu site? Entre em contato com nossa equipe Web da <a href="https://agpagencia.com.br"><strong>AGP Agência</strong></a>.<br><br> Nos envie uma mensagem pelo <a href="https://api.whatsapp.com/send?phone=5511940138315&text=Ol%C3%A1%20AGP%20Ag%C3%AAncia.%20Preciso%20de%20ajuda%20com%20o%20meu%20site!">WhatsApp <b>(11) 9 4013 8315</a></b> ou envie um e-mail <a style="text-decoration: none;" href="mailto:' . antispambot( 'web@agpagencia.com.br' ) . '">clicando aqui!</a></div>';
	}

} // End Class

/**
 * The 'main' function
 *
 * @return void
 */
function agp_custom_main() {
	new agp_custom_class();
}

/**
 * Initialise the plugin
 */
add_action( 'plugins_loaded', 'agp_custom_main' );
